# alimq
阿里mq的demo使用

##项目说明
###mq-api 
存放接口和相关模型
###mq-consumer
edas框架接口的消费端，以及mq发送信息的客户端
###mq-provider
edas框架接口的服务端，以及mq接受信息的服务端

##项目技术说明
采用springboot的框架，需在pom中加入pandora的支持
具体请参考阿里云edas的配置：https://help.aliyun.com/document_detail/55601.html?spm=5176.doc43130.6.629.hgq4sH
