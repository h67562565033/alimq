package com.gooday.mq.service.api;

import com.gooday.mq.model.User;

/**
 * Created by huangyuheng on 2017/8/26.
 */
public interface IUserServiceApi {

    User getUserById(Long id);

}
