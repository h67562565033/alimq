package com.gooday.mq.consumer.controller;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.gooday.mq.common.util.ObjectUtil;
import com.gooday.mq.model.User;
import com.gooday.mq.service.api.IUserServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by huangyuheng on 2017/8/26.
 */
@Controller
@EnableAutoConfiguration
public class IndexController {

    @Autowired
    private IUserServiceApi userServiceApi;

    @Autowired
    private ProducerBean producerBean;

    @RequestMapping("/{id}")
    @ResponseBody
    User index(@PathVariable Long id) {

        User user = userServiceApi.getUserById(id);

        if (null != user) {
            Message msg = new Message("Inmanorder", "*", user.getUsername(),  ObjectUtil.toByteArray(user));
            producerBean.send(msg);
        }

        return user;
    }

}
