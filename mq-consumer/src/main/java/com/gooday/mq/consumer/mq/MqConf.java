package com.gooday.mq.consumer.mq;

import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "mq")
public class MqConf {
	public static final String TOPIC = "Inmanorder";
	public final static String PRODUCER_ID = "PID_inman_producer_1";

	private String accessKey = "LTAIQatOzYHoexoC";
	private String secretKey = "umPUXUpSB2ROrWCsmhcLTkxeoycCrZ";
    
    @Bean
    public ProducerBean normalProducer() {
    	ProducerBean producer = new ProducerBean();
		Properties properties = buildTokenProperties();
		properties.put(PropertyKeyConst.ProducerId, PRODUCER_ID);
		properties.put("Topic", TOPIC);
    	producer.setProperties(properties);
    	producer.start();
    	return producer;
    }
    
	private Properties buildTokenProperties() {
		Properties properties = new Properties();
		properties.put(PropertyKeyConst.AccessKey, this.getAccessKey());
		properties.put(PropertyKeyConst.SecretKey, this.getSecretKey());
		return properties;
	}
    
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
}
