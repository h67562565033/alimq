package com.gooday.mq.provider.service;

import com.gooday.mq.model.User;
import com.gooday.mq.service.api.IUserServiceApi;
import org.springframework.stereotype.Service;

/**
 * Created by huangyuheng on 2017/8/26.
 */
@Service
public class UserServiceImpl implements IUserServiceApi {

    @Override
    public User getUserById(Long id) {

        User user = new User();
        user.setId(id);
        user.setUsername("username" + id);
        user.setAge(Math.toIntExact(id));

        return user;
    }

}
