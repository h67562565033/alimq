package com.gooday.mq.provider.mq;

import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import com.aliyun.openservices.ons.api.bean.Subscription;
import com.gooday.mq.provider.mq.listener.NormalMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "mq")
public class MqConf {
	public static final String CONSUMER_ID = "CID_inman_consumer_1";
	public static final String TOPIC = "Inmanorder";

	private String accessKey = "LTAIQatOzYHoexoC";
    private String secretKey = "umPUXUpSB2ROrWCsmhcLTkxeoycCrZ";
    
    @Autowired
    private NormalMessageListener normalMessageListener;

    @Bean
    public ConsumerBean normalConsumer() {
    	ConsumerBean consumer = new ConsumerBean();
    	Properties properties = buildTokenProperties();
    	properties.put(PropertyKeyConst.ConsumerId, CONSUMER_ID);
    	Subscription subscription = new Subscription();
    	subscription.setTopic(TOPIC);
        subscription.setExpression("*");
        Map<Subscription, MessageListener> subscriptionTable = new HashMap<Subscription, MessageListener>();
        subscriptionTable.put(subscription, normalMessageListener);
        consumer.setProperties(properties);
        consumer.setSubscriptionTable(subscriptionTable);
        consumer.start();
    	return consumer;
    }

	private Properties buildTokenProperties() {
		Properties properties = new Properties();
		properties.put(PropertyKeyConst.AccessKey, this.getAccessKey());
		properties.put(PropertyKeyConst.SecretKey, this.getSecretKey());
		return properties;
	}
    
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
}
