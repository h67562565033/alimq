package com.gooday.mq.provider.mq.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.gooday.mq.common.util.ObjectUtil;
import com.gooday.mq.model.User;
import org.springframework.stereotype.Component;

/**
 * Created by huangyuheng on 2017/8/26.
 */
@Component
public class NormalMessageListener implements MessageListener {

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        // 从打印出来的key可以发现消费是无序的
        System.out.println("Receive Key: " + message.getKey());
        try {
            //do something..

            User user = (User) ObjectUtil.toObject(message.getBody());

            System.out.println(user.toString());

            return Action.CommitMessage;
        } catch (Exception e) {
            //消费失败
            return Action.ReconsumeLater;
        }
    }
}
