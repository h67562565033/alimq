package com.gooday.mq.provider;

import com.taobao.pandora.boot.PandoraBootstrap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by huangyuheng on 2017/8/26.
 */
@SpringBootApplication
//加载HSF配置
@ImportResource(locations = {"classpath:hsf-beans.xml"})
public class MqProviderApplication {

    public static void main(String[] args){

        // 启动Pandora Boot 用于加载Pandora容器
        PandoraBootstrap.run(args);
        SpringApplication.run(MqProviderApplication.class, args);
        // 标记服务启动完成,并设置线程wait。 通常在main函数最后一行调用。
        PandoraBootstrap.markStartupAndWait();

    }

}
